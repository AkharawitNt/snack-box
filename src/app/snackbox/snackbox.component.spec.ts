import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackboxComponent } from './snackbox.component';

describe('SnackboxComponent', () => {
  let component: SnackboxComponent;
  let fixture: ComponentFixture<SnackboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnackboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
