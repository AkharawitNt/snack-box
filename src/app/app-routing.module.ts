import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SnackboxComponent } from './snackbox/snackbox.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'snackbox', component: SnackboxComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
